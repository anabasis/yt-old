# yt
A command-line YouTube client

`yt` is simple way to view, download, and search for YouTube videos, and it depends on [mpv](https://mpv.io) for playing videos.
This is not a very good YouTube client and is very buggy; some issues/bugs include inability to play some videos (500 error), inability to play age-restricted videos, and the deobfuscation caching has some issues (delete the cache file created to fix this).
The videos may also load very slowly (best for audio-only; works great for YouTube Music most of the time).

## Future
* channel navigation
* specify video quality
* search filters (playlist, channel, video, etc.)
* custom search interface
* chapters
* fix mpv displaying `Cache: 0.0s` and make mpv's output less verbose
* config file
* color?
* fix progress bar
* move global flags to global variables
* add more info to view (available countries, family-friendly, tags, unlisted/private)
* change consumer (not just mpv)

## License
`yt` is licensed under MIT License.
