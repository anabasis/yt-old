package main

import (
	"fmt"
	"net/url"
	"os"
	"strings"

	"github.com/ktr0731/go-fuzzyfinder"
	"github.com/tidwall/gjson"
)

type videoInfo struct {
	id          string
	title       string
	channel     string
	description string
	views       string
	date        string
	length      string
}

func PerformSearch(query []string) {
	saneQuery := url.QueryEscape(strings.Join(query, " "))
	var json string
	html := GetHTML("https://www.youtube.com/results?search_query=" + saneQuery)
	json = GetJSON(html, "InitialData")

	videos := gjson.Get(
		json,
		`contents.twoColumnSearchResultsRenderer.primaryContents.sectionListRenderer.contents|@reverse|1.itemSectionRenderer.contents`,
	)

	var slice []videoInfo

	// TODO: fix all of this stuff
	for _, a := range videos.Array() {
		switch {
		case a.Get("videoRenderer").Exists():
			data := a.Get("videoRenderer").Raw
			videoData := gjson.GetMany(
				data,
				"videoId",
				"title.runs.0.text",
				"ownerText.runs.0.text",
				"detailedMetadataSnippets.0.snippetText.runs.0.text",
				"viewCountText.simpleText",
				"publishedTimeText.simpleText",
				"lengthText.simpleText",
			)
			videoStruct := videoInfo{
				videoData[0].String(),
				"VIDEO     " + videoData[1].String(),
				videoData[2].String(),
				videoData[3].String(),
				videoData[4].String(),
				videoData[5].String(),
				videoData[6].String(),
			}
			slice = append(slice, videoStruct)
		case a.Get("playlistRenderer").Exists():
			data := a.Get("playlistRenderer").Raw
			videoData := gjson.GetMany(
				data,
				"playlistId",
				"title.simpleText",
				"longBylineText.runs.0.text",
				"videos.#.childVideoRenderer.title.simpleText",
				"viewCountText.simpleText",
				"publishedTimeText.simpleText",
				"videoCount",
			)
			videoStruct := videoInfo{
				"?list=" + videoData[0].String(),
				"PLAYLIST  " + videoData[1].String(),
				videoData[2].String(),
				"Videos:",
				videoData[4].String(),
				videoData[5].String(),
				videoData[6].String() + " videos",
			}
			videoData[3].ForEach(func(_, value gjson.Result) bool {
				videoStruct.description += "\n" + value.String()
				return true
			})
			slice = append(slice, videoStruct)
		// case a.Get("radioRenderer").Exists():
		// 	data := a.Get("radioRenderer").Raw
		// 	videoData := gjson.GetMany(
		// 		data,
		// 		"playlistId",
		// 		"title.simpleText",
		// 		"longBylineText.simpleText",
		// 		"detailedMetadataSnippets.0.snippetText.runs.0.text",
		// 		"viewCountText.simpleText",
		// 		"publishedTimeText.simpleText",
		// 		"lengthText.simpleText",
		// 	)
		// 	videoStruct := videoInfo{
		// 		"?list=" + videoData[0].String(),
		// 		"PLAYLIST " + videoData[1].String(),
		// 		videoData[2].String(),
		// 		videoData[3].String(),
		// 		videoData[4].String(),
		// 		videoData[5].String(),
		// 		videoData[6].String(),
		// 	}
		// 	slice = append(slice, videoStruct)
		case a.Get("channelRenderer").Exists():
			data := a.Get("channelRenderer").Raw
			videoData := gjson.GetMany(
				data,
				"channelId",
				"title.simpleText",
				"title.simpleText",
				"detailedMetadataSnippets.0.snippetText.runs.0.text",
				"viewCountText.simpleText",
				"publishedTimeText.simpleText",
				"lengthText.simpleText",
			)
			videoStruct := videoInfo{
				"channel/" + videoData[0].String(),
				"CHANNEL   " + videoData[1].String(),
				videoData[2].String(),
				videoData[3].String(),
				videoData[4].String(),
				videoData[5].String(),
				videoData[6].String(),
			}
			slice = append(slice, videoStruct)
		}
	}

	// TODO: use different presenter
	ids, _ := fuzzyfinder.FindMulti(
		slice,
		func(i int) string {
			return fmt.Sprintf("%s (%s)", slice[i].title, slice[i].channel)
		},
		fuzzyfinder.WithPreviewWindow(func(i, _, _ int) string {
			if i == -1 {
				return "no results"
			}
			s := fmt.Sprintf(
				"%s\n\n👤 published by %s\n📅 published %s\n👀 %s\n⏱  %s\n📓 %s",
				slice[i].title,
				slice[i].channel,
				slice[i].date,
				slice[i].views,
				slice[i].length,
				slice[i].description,
			)

			return s
		}),
	)

	if len(ids) > 0 {
		var thingy []string
		for _, id := range ids {
			thingy = append(thingy, slice[id].id)
		}

		if !download {
			PlayVideos(thingy)
		} else {
			DownloadVideos(thingy)
		}
		fmt.Printf("\n")
	} else {
		fmt.Println("No URLS selected!")
		os.Exit(1)
	}
}

func PerformMusicSearch(query []string, searchType string) {
	saneQuery := strings.Join(query, " ")

	data := GetMusicResults(saneQuery, searchType)
	// html := GetHTML("https://music.youtube.com/search?q=" + saneQuery)
	//
	// var err error
	// stringthingy := `"` + strings.ReplaceAll(GetJSON(html, "music"), `\/`, "/") + `"`
	// json, err = strconv.Unquote(stringthingy)
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	videos := gjson.Get(
		data,
		`contents.tabbedSearchResultsRenderer.tabs.0.tabRenderer.content.sectionListRenderer.contents.0.musicShelfRenderer.contents`,
	)

	var slice []videoInfo

	// TODO: fix all of this stuff
	for _, a := range videos.Array() {
		data := a.Get("musicResponsiveListItemRenderer").Raw
		videoData := gjson.GetMany(
			data,
			"playlistItemData.videoId",
			"flexColumns.0.musicResponsiveListItemFlexColumnRenderer.text.runs.0.text",
			"flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs.0.text",
			"flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs.2.text",
			"viewCountText.simpleText",
			"publishedTimeText.simpleText",
			"flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs.4.text",
		)
		videoStruct := videoInfo{
			videoData[0].String(),
			videoData[1].String(),
			videoData[2].String(),
			videoData[3].String(),
			videoData[4].String(),
			videoData[5].String(),
			videoData[6].String(),
		}
		slice = append(slice, videoStruct)
	}

	// TODO: use different presenter
	ids, _ := fuzzyfinder.FindMulti(
		slice,
		func(i int) string {
			return fmt.Sprintf("%s (%s)", slice[i].title, slice[i].channel)
		},
		fuzzyfinder.WithPreviewWindow(func(i, _, _ int) string {
			if i == -1 {
				return "no results"
			}
			s := fmt.Sprintf(
				"%s\n\n👤 published by %s\n⏱  %s\n🎹 %s",
				slice[i].title,
				slice[i].channel,
				slice[i].length,
				slice[i].description,
			)

			return s
		}),
	)

	if len(ids) > 0 {
		var thingy []string
		for _, id := range ids {
			thingy = append(thingy, slice[id].id)
		}

		if !download {
			PlayVideos(thingy)
		} else {
			DownloadVideos(thingy)
		}
		fmt.Printf("\n")
	} else {
		fmt.Println("No URLS selected!")
		os.Exit(1)
	}
}

func ShowTrending() {
	html := GetHTML("https://www.youtube.com/feed/explore")
	json := GetJSON(html, "InitialData")

	videos := gjson.Get(
		json,
		`contents.twoColumnBrowseResultsRenderer.tabs.0.tabRenderer.content.sectionListRenderer.contents|@reverse|0.itemSectionRenderer.contents.0.shelfRenderer.content.expandedShelfContentsRenderer.items.#.videoRenderer`,
	)

	// TODO: include playlists
	var slice []videoInfo

	for _, video := range videos.Array() {
		data := video.String()
		videoData := gjson.GetMany(
			data,
			"videoId",
			"title.runs.0.text",
			"ownerText.runs.0.text",
			"descriptionSnippet.runs.0.text",
			"viewCountText.simpleText",
			"publishedTimeText.simpleText",
			"lengthText.simpleText",
		)
		videoStruct := videoInfo{
			videoData[0].String(),
			videoData[1].String(),
			videoData[2].String(),
			videoData[3].String(),
			videoData[4].String(),
			videoData[5].String(),
			videoData[6].String(),
		}
		slice = append(slice, videoStruct)
	}

	ids, _ := fuzzyfinder.FindMulti(
		slice,
		func(i int) string {
			return fmt.Sprintf("%s (%s)", slice[i].title, slice[i].channel)
		},
		fuzzyfinder.WithPreviewWindow(func(i, _, _ int) string {
			if i == -1 {
				return "no results"
			}
			s := fmt.Sprintf(
				"%s\n\n👤 published by %s\n📅 published %s\n👀 %s\n⏱  %s\n📓 %s",
				slice[i].title,
				slice[i].channel,
				slice[i].date,
				slice[i].views,
				slice[i].length,
				slice[i].description,
			)

			return s
		}),
	)

	if len(ids) > 0 {
		var videos []string
		for _, id := range ids {
			videos = append(videos, "https://www.youtube.com/watch?v="+slice[id].id)
		}

		if !download {
			PlayVideos(videos)
		} else {
			DownloadVideos(videos)
		}
		fmt.Printf("\n")
	} else {
		fmt.Println("No URLS selected!")
		os.Exit(1)
	}
}

func ViewURL(thingURL string) {
	fmt.Println("Viewing:")
	// WARN: check if valid youtube url using regex
	// TODO: move this videoID thing to the actual function???
	videoID := idregex.ReplaceAllString(thingURL, "")
	fmt.Println(videoID)
	switch {
	case strings.Contains(thingURL, "?list="):
		ViewPlaylist(videoID)
	case strings.Contains(thingURL, "channel/"):
		ViewChannel(videoID)
	default:
		ViewVideo(videoID)
	}
}

func ViewVideo(videoID string) {
	json := GetEmbed(videoID)
	if !gjson.Get(json, "streamingData.formats.0.url").Exists() &&
		!gjson.Get(json, "streamingData.hlsManifestUrl").Exists() {
		// !gjson.Get(json, "streamingData.formats.0.signatureCipher").Exists() {
		// fmt.Println("[main]")
		html := GetHTML("https://www.youtube.com/watch?v=" + videoID)
		json = GetJSON(html, "InitialPlayerResponse")
	}

	videoData := gjson.GetMany(
		json,
		"videoDetails.videoId",
		"videoDetails.title",
		"videoDetails.author",
		"videoDetails.shortDescription",
		"videoDetails.viewCount",
		"microformat.playerMicroformatRenderer.publishDate",
		"videoDetails.lengthSeconds",
	)
	videoStruct := videoInfo{
		videoData[0].String(),
		videoData[1].String(),
		videoData[2].String(),
		videoData[3].String(),
		videoData[4].String(),
		videoData[5].String(),
		fmt.Sprintf("%02d:%02d", videoData[6].Int()/60, videoData[6].Int()%60),
	}
	slice := []videoInfo{videoStruct}

	ids, _ := fuzzyfinder.FindMulti(
		slice,
		func(i int) string {
			return fmt.Sprintf("%s (%s)", slice[i].title, slice[i].channel)
		},
		fuzzyfinder.WithPreviewWindow(func(i, _, _ int) string {
			if i == -1 {
				return "no results"
			}
			s := fmt.Sprintf(
				"%s\n\n👤 published by %s\n📅 published %s\n👀 %s\n⏱  %s\n📓 %s",
				slice[i].title,
				slice[i].channel,
				slice[i].date,
				slice[i].views,
				slice[i].length,
				slice[i].description,
			)

			return s
		}),
	)

	if len(ids) > 0 {
		PlayVideos([]string{"https://www.youtube.com/watch?v=" + slice[0].id})
		// if !download {
		// PlayVideos(videos, audioOnly)
		// } else {
		// DownloadVideos(videos, audioOnly)
		// }
		fmt.Printf("\n")
	} else {
		fmt.Println("No URLS selected!")
		os.Exit(1)
	}
}

func ViewChannel(channelID string) {
	html := GetHTML("https://www.youtube.com/channel/" + channelID + "/videos")
	json := GetJSON(html, "InitialData")
	videos := gjson.Get(
		json,
		`contents.twoColumnBrowseResultsRenderer.tabs.1.tabRenderer.content.sectionListRenderer.contents.0.itemSectionRenderer.contents.0.gridRenderer.items.#.gridVideoRenderer`,
	)

	var slice []videoInfo

	for _, video := range videos.Array() {
		data := video.String()
		videoData := gjson.GetMany(
			data,
			"videoId",
			"title.runs.0.text",
			"ownerText.runs.0.text",
			"descriptionSnippet.runs.0.text",
			"viewCountText.simpleText",
			"publishedTimeText.simpleText",
			"lengthText.simpleText",
		)
		videoStruct := videoInfo{
			videoData[0].String(),
			videoData[1].String(),
			videoData[2].String(),
			videoData[3].String(),
			videoData[4].String(),
			videoData[5].String(),
			videoData[6].String(),
		}
		slice = append(slice, videoStruct)
	}

	ids, _ := fuzzyfinder.FindMulti(
		slice,
		func(i int) string {
			return fmt.Sprintf("%s (%s)", slice[i].title, slice[i].channel)
		},
		fuzzyfinder.WithPreviewWindow(func(i, _, _ int) string {
			if i == -1 {
				return "no results"
			}
			s := fmt.Sprintf(
				"%s\n\n👤 published by %s\n📅 published %s\n👀 %s\n⏱  %s\n📓 %s",
				slice[i].title,
				slice[i].channel,
				slice[i].date,
				slice[i].views,
				slice[i].length,
				slice[i].description,
			)

			return s
		}),
	)

	if len(ids) > 0 {
		var videos []string
		for _, id := range ids {
			videos = append(videos, "https://www.youtube.com/watch?v="+slice[id].id)
		}

		PlayVideos(videos)
		// if !download {
		// PlayVideos(videos, audioOnly)
		// } else {
		// DownloadVideos(videos, audioOnly)
		// }
		fmt.Printf("\n")
	} else {
		fmt.Println("No URLS selected!")
		os.Exit(1)
	}
}

func ViewPlaylist(playlistID string) {
	thingy := GetJSON(GetHTML("https://www.youtube.com/embed?list="+playlistID), "cfg")
	videoIDs := gjson.Get(
		thingy,
		"embedded_player_response|@tostr|embedPreview.thumbnailPreviewRenderer.playlist.playlistPanelRenderer.contents.#.playlistPanelVideoRenderer",
	)
	if !videoIDs.Exists() {
		thingy := GetJSON(GetHTML("https://www.youtube.com/playlist?list="+playlistID), "InitialData")
		videoIDs = gjson.Get(
			thingy,
			"contents.twoColumnBrowseResultsRenderer.tabs.0.tabRenderer.content.sectionListRenderer.contents.0.itemSectionRenderer.contents.0.playlistVideoListRenderer.contents.#.playlistVideoRenderer",
		)
	}

	var slice []videoInfo

	for _, video := range videoIDs.Array() {
		data := video.String()
		videoData := gjson.GetMany(
			data,
			"videoId",
			"title.runs.0.text",
			"shortBylineText.runs.0.text",
			"descriptionSnippet.runs.0.text",
			"viewCountText.simpleText",
			"publishedTimeText.simpleText",
			"lengthText.simpleText",
		)
		videoStruct := videoInfo{
			videoData[0].String(),
			videoData[1].String(),
			videoData[2].String(),
			videoData[3].String(),
			videoData[4].String(),
			videoData[5].String(),
			fmt.Sprintf("%02d:%02d", videoData[6].Int()/60, videoData[6].Int()%60),
		}
		slice = append(slice, videoStruct)
	}

	ids, _ := fuzzyfinder.FindMulti(
		slice,
		func(i int) string {
			return fmt.Sprintf("%s (%s)", slice[i].title, slice[i].channel)
		},
		fuzzyfinder.WithPreviewWindow(func(i, _, _ int) string {
			if i == -1 {
				return "no results"
			}
			s := fmt.Sprintf(
				"%s\n\n👤 published by %s\n📅 published %s\n👀 %s\n⏱  %s\n📓 %s",
				slice[i].title,
				slice[i].channel,
				slice[i].date,
				slice[i].views,
				slice[i].length,
				slice[i].description,
			)

			return s
		}),
	)

	if len(ids) > 0 {
		var videos []string
		for _, id := range ids {
			videos = append(videos, "https://www.youtube.com/watch?v="+slice[id].id)
		}

		PlayVideos(videos)
		// if !download {
		// PlayVideos(videos, audioOnly)
		// } else {
		// DownloadVideos(videos, audioOnly)
		// }
		fmt.Printf("\n")
	} else {
		fmt.Println("No URLS selected!")
		os.Exit(1)
	}
}
