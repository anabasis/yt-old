BINARY_NAME=yt
VERSION=v0.1.0

all: install

build:
	go build -trimpath -ldflags="-w -s" -o ${BINARY_NAME} .

build_all:
	mkdir -p ./builds
	rm -f ./builds/*
	GOOS=linux   GOARCH=amd64 go build -trimpath -ldflags="-w -s" -o ./builds/${BINARY_NAME}-linux_amd64 .
	GOOS=darwin  GOARCH=amd64 go build -trimpath -ldflags="-w -s" -o ./builds/${BINARY_NAME}-darwin_amd64 .
	GOOS=windows GOARCH=amd64 go build -trimpath -ldflags="-w -s" -o ./builds/${BINARY_NAME}-windows_amd64 .
	tar -czf ./builds/${BINARY_NAME}-linux_amd64-${VERSION}.tar.gz --owner=0 --group=0 -C ./builds ${BINARY_NAME}-linux_amd64
	zip -j ./builds/${BINARY_NAME}-darwin_amd64-${VERSION}.zip ./builds/${BINARY_NAME}-darwin_amd64
	zip -j ./builds/${BINARY_NAME}-windows_amd64-${VERSION}.zip ./builds/${BINARY_NAME}-windows_amd64
	find . -type f -executable -exec rm '{}' \;

install:
	go install -trimpath -ldflags="-w -s"

upgrade:
	go get -u ./...

test:
	go test -v .

clean:
	go clean
	rm ./builds/${BINARY_NAME}*
	rm ${BINARY_NAME}

lint:
	golangci-lint run --enable-all

vet:
	go vet
