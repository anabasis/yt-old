package main

import (
	"log"
	"os"
	"runtime"

	"github.com/urfave/cli/v2"
)

var (
	audioOnly  bool
	download   bool
	music      bool
	cacheDir   string
	searchType string
)

func main() {
	var err error
	cacheDir, err = os.UserCacheDir()
	if err != nil {
		log.Fatalln(err)
	}

	if runtime.GOOS == "windows" {
		cacheDir += `\yt`
	} else {
		cacheDir += "/yt"
	}

	app := &cli.App{
		Name:    "yt",
		Usage:   "a simple youtube client",
		Version: "v0.2.0",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:        "download",
				Aliases:     []string{"dl", "d"},
				Value:       false,
				Usage:       "download video(s) (progress bar buggy for >1 video downloads)",
				Destination: &download,
			},
			&cli.PathFlag{
				Name:        "cache-dir",
				Value:       cacheDir,
				Usage:       "location for cached file",
				Destination: &cacheDir,
			},
			&cli.StringFlag{
				Name:    "locale",
				Aliases: []string{"l"},
				Value:   "en-US",
				Usage:   "specify language and region (PLACEHOLDER)",
			},
			&cli.BoolFlag{
				Name:        "audio-only",
				Aliases:     []string{"a"},
				Value:       false,
				Usage:       "download/play only audio",
				Destination: &audioOnly,
			},
			&cli.StringFlag{
				Name:    "quality",
				Aliases: []string{"q"},
				Value:   "good",
				Usage:   "quality of videos played/downloaded (PLACEHOLDER)",
			},
		},
		Commands: []*cli.Command{
			{
				Name:      "search",
				Aliases:   []string{"s"},
				Usage:     "perform search",
				ArgsUsage: "[search query]",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "type",
						Aliases:     []string{"t"},
						Value:       "songs",
						Usage:       "display a specified type (only supports YouTube Music)",
						Destination: &searchType,
					},
					&cli.BoolFlag{
						Name:        "music",
						Aliases:     []string{"m"},
						Value:       false,
						Usage:       "search YouTube Music instead of YouTube",
						Destination: &music,
					},
				},
				Action: func(c *cli.Context) error {
					if c.Args().Len() > 0 {
						if !music {
							PerformSearch(c.Args().Slice())
						} else {
							PerformMusicSearch(c.Args().Slice(), searchType)
						}
					} else {
						return cli.Exit("No search query provided!", 1)
					}

					return nil
				},
			},
			{
				Name:      "trending",
				Aliases:   []string{"t"},
				Usage:     "view trending videos",
				ArgsUsage: "",
				Action: func(_ *cli.Context) error {
					ShowTrending()

					return nil
				},
			},
			{
				Name:      "view",
				Aliases:   []string{"v"},
				Usage:     "view information about a video/channel/playlist",
				ArgsUsage: "",
				Action: func(c *cli.Context) error {
					if c.NArg() == 1 {
						ViewURL(c.Args().First())
					} else {
						return cli.Exit("Only one argument accepted!", 1)
					}

					return nil
				},
			},
		},
		Action: func(c *cli.Context) error {
			if c.NArg() > 0 {
				if download {
					DownloadVideos(c.Args().Slice())
				} else {
					PlayVideos(c.Args().Slice())
				}
			} else {
				cli.ShowAppHelp(c)
			}

			return nil
		},
	}
	app.UseShortOptionHandling = true

	err = app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
