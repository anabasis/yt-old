package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"

	"golang.org/x/net/html"
)

var idregex = regexp.MustCompile(
	`(https?://)?(www\.)?(youtu\.be|youtube\.com|music\.youtube\.com|invidious\.snopyta\.org|yewtu\.be|invidious\.kavin\.rocks|vid\.puffyan\.us|invidious\.namazso\.eu|inv\.riverside\.rocks|invidious\.osi\.kr|invidio\.xamh\.de|youtube\.076\.ne\.jp|yt\.artemislena\.eu|inv\.cthd\.icu|invidious\.fdn\.fr|youtube-nocookie\.com|piped\.kavin\.rocks|piped\.silkky\.cloud)?/?(v/|shorts/|watch\?v=|playlist|channel/|embed/?)?(\?list=)?`,
)

func GetBase() string {
	// NOTE: replace this with downloader
	data := GetHTML("https://www.youtube.com/embed/jNQXAC9IVRw")

	tkn := html.NewTokenizer(strings.NewReader(data))

loop:
	for {
		switch tkn.Next() {
		case html.ErrorToken:
			if tkn.Err() == io.EOF {
				break loop
			}
			log.Fatalln(tkn.Err())
		case html.StartTagToken:
			t := tkn.Token()
			if t.Data == "script" {
				for _, x := range t.Attr {
					if x.Key == "src" && strings.HasSuffix(x.Val, "base.js") {
						return GetHTML("https://www.youtube.com" + x.Val)
					}
				}
			}
		}
	}

	return ""
}

func GetHTML(url string) string {
	client := &http.Client{}

	pref := &http.Cookie{
		Name:     "PREF",
		Value:    "tz=UTC&gl=US&hl=en",
		Domain:   ".youtube.com",
		Path:     "/",
		Secure:   true,
		SameSite: http.SameSiteNoneMode,
		HttpOnly: false,
	}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatalln(err)
	}

	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0")
	req.Header.Set("Accept-Language", "en-US,en;q=0.5")
	req.AddCookie(pref)

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	if resp.StatusCode != 200 {
		return ""
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	return string(body)
}

func GetJSON(text string, variable string) string {
	ytvar := fmt.Sprintf("var yt%s", variable)

	var re *regexp.Regexp

	switch variable {
	case "InitialData":
		re = regexp.MustCompile(ytvar + `\s?=\s?|;`)
	case "cfg":
		re = regexp.MustCompile(ytvar + `.*|.*"PLAYER_VARS":|\);.*`)
	case "InitialPlayerResponse":
		re = regexp.MustCompile(ytvar + `\s?=\s?|;var meta.*`)
	case "music":
		re = regexp.MustCompile(
			`try \{const initialData = \[\];.*, data: '|'\}\);ytcfg\.set\(\{'YTMUSIC_INITIAL_DATA':.*|\n`,
		)
	}

	tkn := html.NewTokenizer(strings.NewReader(text))

	var isScript bool

	for {
		switch tkn.Next() {
		case html.StartTagToken:
			t := tkn.Token()
			isScript = t.Data == "script"
		case html.TextToken:
			t := tkn.Token()
			if isScript && strings.HasPrefix(t.Data, ytvar) {
				return re.ReplaceAllString(t.Data, "")
			}
		}
	}
}

func GetScript(text string) string {
	tkn := html.NewTokenizer(strings.NewReader(text))
	var isScript bool

	for {
		switch tt := tkn.Next(); tt {
		case html.StartTagToken:
			t := tkn.Token()
			isScript = t.Data == "script"
			if isScript && strings.HasSuffix(t.Attr[0].Val, "base.js") {
				return "https://youtube.com" + t.Attr[0].Val
			}
		}
	}
}

func GetEmbed(videoID string) string {
	client := &http.Client{}

	// WARN: innertube api can change at anytime; make this future-proof
	// WARN: client version does not change, making this look suspicious
	req, err := http.NewRequest(
		"POST",
		"https://www.youtube.com/youtubei/v1/player?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8",
		bytes.NewBufferString(
			// `{"videoId":"`+videoID+`","context":{"client":{"clientName":"TVHTML5_SIMPLY_EMBEDDED_PLAYER","clientVersion":"2.0"}}}`,
			// `{"videoId":"`+videoID+`","context":{"client":{"clientName":"WEB_EMBEDDED_PLAYER","clientVersion":"2.20220325.00.00"}}}`,
			`{"videoId":"`+videoID+`","context":{"client":{"clientName":"WEB_EMBEDDED_PLAYER","clientVersion":"1.20220517.01.01"}}}`,
		),
	)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0")
	req.Header.Set("Accept-Language", "en-US,en;q=0.5")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	return string(body)
}

func GetMusicResults(query string, searchType string) string {
	client := &http.Client{}
	searchType = strings.ToLower(searchType)

	var param string
	switch searchType {
	case "featured playlist":
	case "featured playlists":
		param = "EgeKAQQoADgBagwQAxAOEAkQBRAEEAo%3D"
	case "album":
	case "albums":
		param = "EgWKAQIYAWoMEAMQDhAJEAUQBBAK"
	case "artist":
	case "artists":
		param = "EgWKAQIgAWoMEAMQDhAJEAUQBBAK"
	case "video":
	case "videos":
		param = "EgWKAQIQAWoMEAMQDhAJEAUQBBAK"
	case "community playlist":
	case "community playlists":
		param = "EgeKAQQoAEABagwQAxAOEAkQBRAEEAo%3D"
	case "song":
	case "songs":
		param = "EgWKAQIIAWoMEAMQDhAJEAUQBBAK"
	default:
		param = "EgWKAQIIAWoMEAMQDhAJEAUQBBAK"
	}

	// WARN: innertube api can change at anytime; make this future-proof
	req, err := http.NewRequest(
		"POST",
		"https://music.youtube.com/youtubei/v1/search?key=AIzaSyC9XL3ZjWddXya6X74dJoCTL-WEYFDNX30&prettyPrint=false",
		bytes.NewBufferString(
			`{"context":{"client":{"clientName":"WEB_REMIX","clientVersion":"1.20220321.01.00"}},"query":"`+query+`","params":"`+param+`"}`,
		),
	)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0")
	req.Header.Set("Accept-Language", "en-US,en;q=0.5")
	req.Header.Set("Referer", "https://music.youtube.com/")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	return string(body)
}
