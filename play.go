package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"syscall"

	"github.com/tidwall/gjson"
)

func PlayVideos(idek []string) {
	fmt.Println("Playing:")
	urlschan := make(chan []string, len(idek))
	for _, thing := range idek {
		// WARN: check if valid youtube url using regex
		// TODO: move this videoID thing to the actual function???
		videoID := idregex.ReplaceAllString(thing, "")
		// TODO: add function for channels
		switch {
		case strings.Contains(thing, "?list="):
			fmt.Println("https://www.youtube.com/playlist?list=" + videoID)
			go GetPlaylist(videoID, urlschan)
		case strings.Contains(thing, "channel/"):
			fmt.Printf("Skipping %s because it is a channel URL.\n", "https://www.youtube.com/channel/"+videoID)
			urlschan <- []string{}
		default:
			fmt.Println("https://www.youtube.com/watch?v=" + videoID)
			go GetVideoUrl(videoID, urlschan)
		}
	}

	var urls []string
	// BUG: videos are played out of order and stuff
	for i := 0; i < len(idek); i++ {
		urls = append(urls, <-urlschan...)
	}

	binary, lookErr := exec.LookPath("mpv")
	if lookErr != nil {
		panic(lookErr)
	}

	args := []string{"mpv", "--user-agent=Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"}
	if audioOnly {
		args = append(args, "--no-video")
	}
	args = append(args, urls...)

	if len(urls) > 0 {
		env := os.Environ()
		execErr := syscall.Exec(binary, args, env)
		if execErr != nil {
			panic(execErr)
		}
	}
}

func GetVideoUrl(videoID string, ch chan []string) {
	// WARN: fix music videos on embed (is this working properly???)
	// BUG: 416 errors occur
	// BUG: 503 errors occur
	// HACK: better handling logic for the quality

	// json := GetEmbed(videoID)
	var json string
	// log.Println("[embed]")

	if !gjson.Get(json, "streamingData.formats.0.url").Exists() &&
		!gjson.Get(json, "streamingData.hlsManifestUrl").Exists() {
		// !gjson.Get(json, "streamingData.formats.0.signatureCipher").Exists() {
		// log.Println("[main]")
		html := GetHTML("https://www.youtube.com/watch?v=" + videoID)
		if html == "" {
			log.Println("Downloading", videoID, "failed")
			ch <- nil
		}
		json = GetJSON(html, "InitialPlayerResponse")
	}

	title := gjson.Get(json, "videoDetails.title").String()
	var videoURL string
	var audioURL string

	switch {
	case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="480p").url`).Exists():
		videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="480p").url`).String()
	case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="360p").url`).Exists():
		videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="360p").url`).String()
	case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="240p").url`).Exists():
		videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="240p").url`).String()
	case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="144p").url`).Exists():
		videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="144p").url`).String()

	case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="480s").url`).Exists():
		videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="480").url`).String()
	case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="360s").url`).Exists():
		videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="360s").url`).String()
	case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="240s").url`).Exists():
		videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="240s").url`).String()
	case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="144s").url`).Exists():
		videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="144s").url`).String()
	}

	audioURL = gjson.Get(json, `streamingData.adaptiveFormats.#(audioQuality=="AUDIO_QUALITY_MEDIUM").url`).String()
	if audioURL == "" {
		audioURL = gjson.Get(json, `streamingData.adaptiveFormats.#(audioQuality=="AUDIO_QUALITY_LOW").url`).String()
	}

	if videoURL == "" && gjson.Get(json, "streamingData.formats.0.signatureCipher").Exists() {
		// check if video has obfuscated URL

		// fmt.Println(GetHTML(GetScript(html)))
		// TODO: find function name with regex, find contents of function, find variable "aA" or "xZ", parse the function
		// OPTIM: add concurrency
		// HACK: make quality handling logic better

		switch {
		case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="480p").signatureCipher`).Exists():
			videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="480p").signatureCipher`).
				String()
		case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="360p").signatureCipher`).Exists():
			videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="360p").signatureCipher`).
				String()
		case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="240p").signatureCipher`).Exists():
			videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="240p").signatureCipher`).
				String()
		case gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="144p").signatureCipher`).Exists():
			videoURL = gjson.Get(json, `streamingData.adaptiveFormats.#(qualityLabel=="144p").signatureCipher`).
				String()
		}
		audioURL = gjson.Get(json, `streamingData.adaptiveFormats.#(audioQuality=="AUDIO_QUALITY_MEDIUM").signatureCipher`).
			String()

		version := gjson.Get(json, `responseContext.serviceTrackingParams.#(service=="CSI").params.#(key=="cver").value`).
			String()

		videoURL = DeobfuscateURL(videoURL, version)
		audioURL = DeobfuscateURL(audioURL, version)
	} else if gjson.Get(json, "streamingData.hlsManifestUrl").Exists() {
		// check if video is a livestream
		videoURL = gjson.Get(json, "streamingData.hlsManifestUrl").String()
		audioURL = ""
	}

	subtitles := gjson.Get(json, "captions.playerCaptionsTracklistRenderer.captionTracks.#.baseUrl").Array()
	var subtitleURLs []string
	for _, file := range subtitles {
		subtitleURLs = append(subtitleURLs, "--sub-file="+file.String()+"&fmt=vtt")
	}

	switch {
	case videoURL == "":
		// videos with errors
		fmt.Printf(
			"Skipping %s because of the following error: %s\n",
			videoID,
			gjson.Get(json, "playabilityStatus.reason").String(),
		)
		ch <- nil
	case len(subtitles) == 0 && audioURL == "":
		// livestreams
		ch <- []string{"--{", videoURL, "--force-media-title=" + title, "--}"}
	case len(subtitles) == 0 && audioURL != "":
		// videos without captions
		ch <- []string{"--{", videoURL, "--audio-file=" + audioURL, "--force-media-title=" + title, "--}"}
	default:
		// videos with captions
		out := []string{"--{", videoURL, "--audio-file=" + audioURL, "--force-media-title=" + title}
		out = append(out, subtitleURLs...)
		ch <- append(out, "--}")
	}
}

func GetPlaylist(playlistID string, ch chan []string) {
	thingy := GetJSON(GetHTML("https://www.youtube.com/embed?list="+playlistID), "cfg")
	videoIDs := gjson.Get(
		thingy,
		"embedded_player_response|@tostr|embedPreview.thumbnailPreviewRenderer.playlist.playlistPanelRenderer.contents.#.playlistPanelVideoRenderer.videoId",
	).Array()
	if len(videoIDs) == 0 {
		thingy := GetJSON(GetHTML("https://www.youtube.com/playlist?list="+playlistID), "InitialData")
		videoIDs = gjson.Get(
			thingy,
			"contents.twoColumnBrowseResultsRenderer.tabs.0.tabRenderer.content.sectionListRenderer.contents.0.itemSectionRenderer.contents.0.playlistVideoListRenderer.contents.#.playlistVideoRenderer.videoId",
		).Array()
	}
	urlschan := make(chan []string, len(videoIDs))

	for _, video := range videoIDs {
		go GetVideoUrl(video.String(), urlschan)
	}

	var out []string
	// BUG: videos are played out of order and stuff
	for i := 0; i < len(videoIDs); i++ {
		out = append(out, <-urlschan...)
	}
	ch <- out
}
