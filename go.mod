module yt

go 1.17

require golang.org/x/net v0.5.0 // direct

require (
	github.com/tidwall/gjson v1.14.4 // direct
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/gdamore/tcell/v2 v2.5.4 // indirect
	github.com/ktr0731/go-fuzzyfinder v0.7.0 // direct
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.4.3 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/term v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
)

require (
	github.com/robertkrimen/otto v0.2.1
	github.com/schollz/progressbar/v3 v3.13.0
	github.com/urfave/cli/v2 v2.24.1
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/ktr0731/go-ansisgr v0.1.0 // indirect
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
)
