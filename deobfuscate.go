package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"regexp"
	"runtime"

	"github.com/robertkrimen/otto"
)

type cacheFileFormat struct {
	DeobFunc string `json:"deobFunc"`
	Variable string `json:"variable"`
}

func DeobfuscateURL(streamURL string, version string) string {
	parsedURL, err := url.ParseQuery(streamURL)
	if err != nil {
		log.Fatalln(err)
	}

	dirInfo, err := os.Stat(cacheDir)
	if os.IsNotExist(err) {
		err = os.Mkdir(cacheDir, 0o755)
		if err != nil {
			log.Fatalln(err)
		}
	} else {
		if !dirInfo.IsDir() {
			log.Fatalln(cacheDir + " is not a directory")
		}
	}

	var cacheFile string
	if runtime.GOOS == "windows" {
		cacheFile = cacheDir + `\yt_cache`
	} else {
		cacheFile = cacheDir + "/yt_cache"
	}

	var cipher cacheFileFormat

	if _, err := os.Stat(cacheFile); os.IsNotExist(err) {
		if _, err = os.Create(cacheFile); err != nil {
			log.Fatalln(err)
		}

		cipher = DeobfuscateFunction(version, cacheFile)
	} else {
		file, err := ioutil.ReadFile(cacheFile)
		if err != nil {
			log.Fatalln(err)
		}

		fileData := map[string]cacheFileFormat{}
		json.Unmarshal(file, &fileData)

		cipher = fileData[version]
		if (cipher == cacheFileFormat{}) {
			cipher = DeobfuscateFunction(version, cacheFile)
		}
	}

	vm := otto.New()
	vm.Run(cipher.Variable + "var deobfuscateFunc = " + cipher.DeobFunc)
	ottoDeobSig, _ := vm.Call("deobfuscateFunc", nil, parsedURL.Get("s"))
	signature, _ := ottoDeobSig.ToString()

	return parsedURL.Get("url") + "&" + parsedURL.Get("sp") + "=" + signature
}

func DeobfuscateFunction(version string, cacheFile string) cacheFileFormat {
	file, err := os.ReadFile(cacheFile)
	if err != nil {
		log.Fatalln(err)
	}

	fileData := map[string]cacheFileFormat{}

	json.Unmarshal(file, &fileData)

	data := GetBase()

	// TODO: error handling
	deobFunction := regexp.MustCompile(`function\(a\)\{a=a.split\(""\);.+?;return a.join\(""\)\};`).FindString(data)
	deobVarName := regexp.MustCompile(`a.split\(""\);([A-Za-z$_][0-9A-Za-z$_]{1,2})`).FindStringSubmatch(deobFunction)[1]
	deobObj := regexp.MustCompile(`var ` + regexp.QuoteMeta(deobVarName) + `=\{.+?\n?.+?\n?.+?\}\};`).FindString(data)

	deobData := cacheFileFormat{deobFunction, deobObj}

	fileData[version] = deobData

	out, err := json.Marshal(fileData)
	if err != nil {
		log.Fatalln(err)
	}

	os.WriteFile(cacheFile, out, 0o644)

	return deobData
}
