package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/schollz/progressbar/v3"
	"github.com/tidwall/gjson"
)

func DownloadVideos(idek []string) {
	done := make(chan bool, len(idek))

	fmt.Println("Downloading:")
	for _, thing := range idek {
		// WARN: check if valid youtube url using regex
		// TODO: move this videoID thing to the actual function???
		videoID := idregex.ReplaceAllString(thing, "")
		// TODO: add functions for playlists and channels
		switch {
		case strings.Contains(thing, "?list="):
			fmt.Println("https://www.youtube.com/playlist?list=" + videoID)
			go GetPlaylistForDl(videoID, done)
		case strings.Contains(thing, "channel/"):
			fmt.Printf("Skipping %s because it is a channel URL.\n", "https://www.youtube.com/channel/"+videoID)
		default:
			fmt.Println("https://www.youtube.com/watch?v=" + videoID)
			go GetVideoForDl(videoID, done)
			fmt.Printf("\n")
		}
	}

	for i := 0; i < len(idek); i++ {
		<-done
	}

	fmt.Println("Finished downloading!")
}

func GetVideoForDl(videoID string, done chan bool) error {
	// TODO: use adaptive formats for better performance
	// TODO: rewrite progress indicator thing (content-length and support for concurrent downloads)
	json := GetEmbed(videoID)
	// fmt.Println("[embed]")
	if gjson.Get(json, "streamingData.formats.0.url").String() == "" {
		// fmt.Println("[main]")
		html := GetHTML("https://www.youtube.com/watch?v=" + videoID)
		json = GetJSON(html, "InitialPlayerResponse")
	}

	var video gjson.Result
	if !audioOnly {
		video = gjson.Get(json, "streamingData.formats|@reverse|0")
	} else {
		video = gjson.Get(json, "streamingData.adaptiveFormats.#(audioQuality==\"AUDIO_QUALITY_MEDIUM\")")
	}

	videoURL := video.Get("url").String()

	if videoURL == "" && video.Get("signatureCipher").Exists() {
		// OPTIM: add concurrency

		videoURL = video.Get("signatureCipher").String()
		version := gjson.Get(json, `responseContext.serviceTrackingParams.#(service=="CSI").params.#(key=="cver").value`).
			String()

		videoURL = DeobfuscateURL(videoURL, version)
	} else if gjson.Get(json, "streamingData.hlsManifestUrl").Exists() {
		fmt.Println("Skipping " + videoID + " because it is a livestream.")
		done <- true

		return nil
	}

	if videoURL == "" {
		fmt.Printf(
			"Skipping " + videoID + " because the following error was present: " + gjson.Get(json, "playabilityStatus.reason").
				String() +
				"\n\n",
		)
		done <- true
	}

	title := gjson.Get(json, "videoDetails.title").String()
	re := regexp.MustCompile(`.*/|;.*`)
	fileExt := "." + re.ReplaceAllString(video.Get("mimeType").String(), "")

	filepath := strings.ReplaceAll(title, "/", "_") + "-" + videoID + fileExt
	out, err := os.Create(filepath + ".tmp")
	if err != nil {
		done <- true

		return err
	}

	resp, err := http.Get(videoURL)
	if err != nil {
		out.Close()
		done <- true

		return err
	}
	defer resp.Body.Close()

	// PROGRESSBAR
	bar := progressbar.DefaultBytes(
		resp.ContentLength,
		"downloading",
	)
	io.Copy(io.MultiWriter(out, bar), resp.Body)

	// Close the file without defer so it can happen before Rename()
	out.Close()

	if err = os.Rename(filepath+".tmp", filepath); err != nil {
		done <- true
		return err
	}
	done <- true

	return nil
}

func GetPlaylistForDl(playlistID string, done chan bool) {
	thingy := GetJSON(GetHTML("https://www.youtube.com/embed?list="+playlistID), "cfg")
	videoIDs := gjson.Get(
		thingy,
		"embedded_player_response|@tostr|embedPreview.thumbnailPreviewRenderer.playlist.playlistPanelRenderer.contents.#.playlistPanelVideoRenderer.videoId",
	).Array()
	if len(videoIDs) == 0 {
		thingy := GetJSON(GetHTML("https://www.youtube.com/playlist?list="+playlistID), "InitialData")
		videoIDs = gjson.Get(
			thingy,
			"contents.twoColumnBrowseResultsRenderer.tabs.0.tabRenderer.content.sectionListRenderer.contents.0.itemSectionRenderer.contents.0.playlistVideoListRenderer.contents.#.playlistVideoRenderer.videoId",
		).Array()
	}
	donechan := make(chan bool, len(videoIDs))

	for _, video := range videoIDs {
		go GetVideoForDl(video.String(), donechan)
	}

	for i := 0; i < len(videoIDs); i++ {
		<-donechan
	}
	done <- true
}
